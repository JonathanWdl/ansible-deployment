# Ansible Provisioning

This is an ansible setup to deploy TYPO3 Applications.
Two playbooks are given:
* provision.yml
* deploy.yml

Run the provision script with:
```sh
ansible-playbook -i inventories/staging provision.yml
```

Run the deploy script with:
```sh
ansible-playbook -i inventories/staging deploy.yml 
```